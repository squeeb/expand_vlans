#!/usr/bin/env python3
import sys

def expand_vlan_set(vlan_list):
    flattened_vlan_list = vlan_list.replace('\n','').replace(' ','')
    split_vlan_list = vlan_list.split(',')

    vlans = []

    for vlan_element in split_vlan_list:
        if '-' in vlan_element:
            vlan_range = vlan_element.split('-')

            [vlans.append(vlan) for vlan in range(
                int(vlan_range[0]),
                int(vlan_range[1])+1
            )]

        else:
            vlans.append(int(vlan_element))

    return vlans

def get_differences(set_a, set_b, symmetric=True):
    set_a = set(set_a)
    set_b = set(set_b)

    if symmetric:
        return list(set_a.symmetric_difference(set_b))
    else:
        return list(set_a.difference(set_b))

if __name__ == '__main__':

    if len(sys.argv) != 3:
        raise Exception("Two VLAN sets required")

    vlan_list_a = expand_vlan_set(sys.argv[1])
    vlan_list_b = expand_vlan_set(sys.argv[2])

    differences = get_differences(vlan_list_a, vlan_list_b)

    for vlan_id in differences:
        if vlan_id in vlan_list_a:
            print("mismatched VLAN %d present on switch A and not on switch B" % vlan_id)
        if vlan_id in vlan_list_b:
            print("mismatched VLAN %d present on switch B and not on switch A" % vlan_id)
